﻿using Android.App;
using Android.Content.PM;
using Android.OS;

namespace Telemedicine
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : BaseNavigationActivity
    {
        /// <summary>
        /// Layout id
        /// </summary>
        protected override int LayoutId => Resource.Layout.activity_main;

        protected override int ActiveMenuId => Resource.Id.navigation_home;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }
    }
}

