﻿using Android.App;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Util;
using Android.Widget;
using GR.Core.Helpers;
using System;
using System.Globalization;
using Telemedicine.Helpers;

namespace Telemedicine
{
    [Activity(Label = "Doctor Details", Theme = "@style/AppTheme", ScreenOrientation = ScreenOrientation.Portrait)]
    public class DoctorDetailsActivity : BaseNavigationActivity
    {
        /// <summary>
        /// Client
        /// </summary>
        private readonly TelemedicineHttpClient _client;

        private int DoctorId { get; set; }

        public DoctorDetailsActivity()
        {
            _client = IoC.Resolve<TelemedicineHttpClient>();
        }

        /// <summary>
        /// Layout id
        /// </summary>
        protected override int LayoutId => Resource.Layout.DoctorDetails;

        protected override int ActiveMenuId => Resource.Id.navigation_add;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            DoctorId = (int)Intent.Extras.Get("DoctorId");
            var doctorRequest = await _client.GetDoctorByIdAsync(DoctorId);
            if (!doctorRequest.IsSuccess) return;
            var doctor = doctorRequest.Result;
            var imageView = FindViewById<ImageView>(Resource.Id.photo);
            var decodedBytes = Base64.Decode(doctor.Photo.Substring(doctor.Photo.IndexOf(",", StringComparison.Ordinal) + 1),
                Base64Flags.Default);

            var bitmap = BitmapFactory.DecodeByteArray(decodedBytes, 0, decodedBytes.Length);
            imageView.SetImageBitmap(bitmap);


            var fullNameView = FindViewById<TextView>(Resource.Id.fullName);
            fullNameView.Text = doctor.FullName;


            var specsView = FindViewById<TextView>(Resource.Id.specs);
            specsView.Text = doctor.Specs;

            var aboutView = FindViewById<TextView>(Resource.Id.doctor_about);
            aboutView.Text = doctor.About;


            var addressView = FindViewById<TextView>(Resource.Id.address);
            addressView.Text = doctor.Address;


            var ratingView = FindViewById<TextView>(Resource.Id.rating);
            ratingView.Text = doctor.Stars.ToString(CultureInfo.InvariantCulture);

            var starLayout = FindViewById<LinearLayout>(Resource.Id.star_layout);

            for (var i = 0; i < starLayout.ChildCount - 1; i++)
            {
                var star = starLayout.GetChildAt(i);
                if (star is TextView starView)
                {
                    var resource = (int)doctor.Stars >= i + 1 ? Resource.Drawable.star : Resource.Drawable.star_gray;
                    star.SetBackgroundResource(resource);
                }
            }
        }
    }
}