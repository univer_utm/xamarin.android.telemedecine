﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Widget;
using GR.Core.Helpers;
using Telemedicine.Helpers;
using Telemedicine.Models;

namespace Telemedicine
{
    [Activity(Label = "Login", ScreenOrientation = ScreenOrientation.Portrait)]
    public class Login : Activity
    {
        private readonly TelemedicineHttpClient _client;

        public Login()
        {
            _client = IoC.Resolve<TelemedicineHttpClient>();
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            IoC.Resolve<DrawHelper>().SetBarColor(this, Settings.BarColor);
            SetContentView(Resource.Layout.Login);
            var signupBtn = FindViewById<TextView>(Resource.Id.signup);
            signupBtn.Click += (sender, args) =>
           {
               var intent = new Intent(this, typeof(Signup));
               StartActivity(intent);
           };

            var loginBtn = FindViewById<Button>(Resource.Id.save);
            loginBtn.Click += async (sender, args) =>
            {
                var email = FindViewById<EditText>(Resource.Id.email).Text;
                var password = FindViewById<EditText>(Resource.Id.password).Text;

                var response = await _client.LoginAsync(new LoginModel
                {
                    //Email = "nicolae.lupei.1996@gmail.com",
                    //Password = "Master.1996"
                    Email = email,
                    Password = password
                });

                if (response.IsSuccess)
                {
                    var intent = new Intent(this, typeof(MainActivity));
                    StartActivity(intent);
                }
                else
                {
                    Toast.MakeText(this, response.Result.Message, ToastLength.Long).Show();
                }
            };
        }
    }
}