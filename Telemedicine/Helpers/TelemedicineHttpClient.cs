﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using GR.Core.Extensions;
using GR.Core.Helpers;
using GR.Core.Helpers.Responses;
using Telemedicine.Extensions;
using Telemedicine.Models;

namespace Telemedicine.Helpers
{
    public class TelemedicineHttpClient : HttpClient
    {
        public TelemedicineHttpClient()
        {
            BaseAddress = new Uri(Settings.URL);
            DefaultRequestHeaders.Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
            DefaultRequestHeaders.Add("Token", Settings.Token);
        }

        /// <summary>
        /// Register
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResultModel<LoginResultModel>> RegisterAsync(RegisterModel model)
        {
            var response = await this.SendFormUrlEncodedAsync(HttpMethod.Post, Settings.Routes.RegisterPath, model);
            var result = await response.Content.ReadAsJsonAsync<LoginResultModel>();

            if (response.IsSuccessStatusCode)
            {
                return new SuccessResultModel<LoginResultModel>(result);
            }

            return new ResultModel<LoginResultModel>
            {
                Result = result
            };
        }

        /// <summary>
        /// Login
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResultModel<LoginResultModel>> LoginAsync(LoginModel model)
        {
            var response = await this.SendFormUrlEncodedAsync(HttpMethod.Post, Settings.Routes.LoginPath, model);
            if (!response.IsSuccessStatusCode) return new ResultModel<LoginResultModel>
            {
                Result = new LoginResultModel
                {
                    Message = "Invalid parameters"
                }
            };
            var result = await response.Content.ReadAsJsonAsync<LoginResultModel>();
            if (result.Status.Equals("SUCCESS"))
            {
                Settings.Token = result.Message;
                DefaultRequestHeaders.Remove("Token");
                DefaultRequestHeaders.Add("Token", Settings.Token);
                return new SuccessResultModel<LoginResultModel>(result);
            }

            return new InvalidParametersResultModel<LoginResultModel>();
        }

        /// <summary>
        /// Get profile
        /// </summary>
        /// <returns></returns>
        public async Task<ResultModel<ProfileModel>> GetProfileAsync()
        {
            var response = await GetAsync(Settings.Routes.ProfilePath);
            var result = await response.Content.ReadAsJsonAsync<ProfileModel>();
            if (!response.IsSuccessStatusCode) return new InvalidParametersResultModel<ProfileModel>();
            return new SuccessResultModel<ProfileModel>(result);
        }

        /// <summary>
        /// Get doctor list
        /// </summary>
        /// <returns></returns>
        public async Task<ResultModel<IEnumerable<DoctorModel>>> GetDoctorListAsync()
        {
            var response = await GetAsync(Settings.Routes.DoctorListPath);
            if (!response.IsSuccessStatusCode) return new InvalidParametersResultModel<IEnumerable<DoctorModel>>();
            var result = await response.Content.ReadAsJsonAsync<DoctorModel[]>();
            return new SuccessResultModel<IEnumerable<DoctorModel>>(result);
        }

        /// <summary>
        /// Get docctor by id
        /// </summary>
        /// <returns></returns>
        public async Task<ResultModel<DoctorModel>> GetDoctorByIdAsync(int doctorId)
        {
            var response = await GetAsync($"{Settings.Routes.DoctorByIdPath}/{doctorId}");
            if (!response.IsSuccessStatusCode) return new InvalidParametersResultModel<DoctorModel>();
            var result = await response.Content.ReadAsJsonAsync<DoctorModel>();
            return new SuccessResultModel<DoctorModel>(result);
        }
    }
}