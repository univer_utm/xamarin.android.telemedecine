﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Util;
using Android.Views;
using Android.Widget;
using Telemedicine.Models;

namespace Telemedicine.Helpers
{
    public class DoctorListAdapter : ArrayAdapter<DoctorModel>, IFilterable
    {
        private readonly Activity _activity;
        private readonly int layoutResourceId;
        private readonly IEnumerable<DoctorModel> _data = new List<DoctorModel>();

        public DoctorListAdapter(Activity activity, int resource, List<DoctorModel> items) : base(activity, resource, items)
        {
            this.layoutResourceId = resource;
            _activity = activity;
            _data = items;
        }


        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView ?? _activity.LayoutInflater.Inflate(Resource.Layout.DoctorListItem, parent, false);
            var o = _data.ElementAt(position);
            var imageView = view.FindViewById<ImageView>(Resource.Id.photo);
            var decodedBytes = Base64.Decode(o.Photo.Substring(o.Photo.IndexOf(",", StringComparison.Ordinal) + 1),
                Base64Flags.Default);

            var bitmap = BitmapFactory.DecodeByteArray(decodedBytes, 0, decodedBytes.Length);
            imageView.SetImageBitmap(bitmap);


            var ratingView = view.FindViewById<TextView>(Resource.Id.rating);
            ratingView.Text = o.Stars.ToString(CultureInfo.InvariantCulture);


            var fullNameView = view.FindViewById<TextView>(Resource.Id.fullName);
            fullNameView.Text = o.FullName;


            var specsView = view.FindViewById<TextView>(Resource.Id.specs);
            specsView.Text = o.Specs;


            var locationView = view.FindViewById<TextView>(Resource.Id.address);
            locationView.Text = o.Address.Length > 15 ? o.Address.Substring(0, 14) + "..." : o.Address;

            view.Click += (obj, args) =>
            {
                var doctorDetailsIntent = new Intent(_activity, typeof(DoctorDetailsActivity));
                doctorDetailsIntent.PutExtra("DoctorId", o.DocId);
                _activity.StartActivity(doctorDetailsIntent);
            };
            return view;
        }
    }
}