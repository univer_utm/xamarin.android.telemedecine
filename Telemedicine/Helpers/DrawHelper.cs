﻿using Android.Graphics;
using Android.OS;
using Android.Views;
using Android.App;

namespace Telemedicine.Helpers
{
    public class DrawHelper
    {
        public void SetBarColor<T>(T activity, string color)
            where T : Activity
        {
            if (Build.VERSION.SdkInt < BuildVersionCodes.Lollipop) return;
            activity.Window.ClearFlags(WindowManagerFlags.TranslucentStatus);
            activity.Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
            activity.Window.SetStatusBarColor(Color.ParseColor(color));
        }
    }
}