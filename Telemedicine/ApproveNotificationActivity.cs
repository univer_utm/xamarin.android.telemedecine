﻿
using Android.App;
using Android.OS;

namespace Telemedicine
{
    [Activity(Label = "Approve Notification")]
    public class ApproveNotificationActivity : BaseNavigationActivity
    {
        protected override int LayoutId => Resource.Layout.ApproveNotification;

        protected override int ActiveMenuId => Resource.Id.navigation_notifications;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }
    }
}