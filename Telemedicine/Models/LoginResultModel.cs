﻿namespace Telemedicine.Models
{
    public class LoginResultModel
    {
        public string Status { get; set; }
        public string Message { get; set; }
    }
}