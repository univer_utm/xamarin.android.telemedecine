﻿namespace Telemedicine.Models
{
    public class DoctorModel
    {
        public int DocId { get; set; }
        public string FullName { get; set; }
        public string Specs { get; set; }
        public string Address { get; set; }
        public string About { get; set; }
        public decimal Stars { get; set; }
        public string Photo { get; set; }
    }
}