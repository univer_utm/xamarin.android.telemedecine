﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Widget;
using GR.Core.Helpers;
using Telemedicine.Helpers;
using Telemedicine.Models;

namespace Telemedicine
{
    [Activity(Label = "Signup", ScreenOrientation = ScreenOrientation.Portrait)]
    public class Signup : Activity
    {
        private readonly TelemedicineHttpClient _client;

        public Signup()
        {
            _client = IoC.Resolve<TelemedicineHttpClient>();
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            IoC.Resolve<DrawHelper>().SetBarColor(this, Settings.BarColor);
            SetContentView(Resource.Layout.Signup);

            var registerButton = FindViewById<Button>(Resource.Id.register);
            registerButton.Click += async (sender, args) =>
            {
                var email = FindViewById<EditText>(Resource.Id.email).Text;
                var dateStr = FindViewById<EditText>(Resource.Id.birthDay).Text;
                var model = new RegisterModel
                {
                    FullName = FindViewById<EditText>(Resource.Id.fullName).Text,
                    BirthDay = dateStr,
                    Email = email,
                    UserName = email,
                    Phone = FindViewById<EditText>(Resource.Id.phone).Text,
                    Address = FindViewById<EditText>(Resource.Id.location).Text,
                    Password = FindViewById<EditText>(Resource.Id.password).Text,
                };

                var result = await _client.RegisterAsync(model);

                if (result.IsSuccess)
                {
                    var loginResult = await _client.LoginAsync(new LoginModel
                    {
                        Email = model.Email,
                        Password = model.Password
                    });

                    if (!loginResult.IsSuccess) return;
                    var intent = new Intent(this, typeof(MainActivity));
                    StartActivity(intent);
                }
                else
                {
                    Toast.MakeText(this, result.Result.Message, ToastLength.Long).Show();
                }
            };
        }
    }
}