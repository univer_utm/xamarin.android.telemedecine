﻿using System.Net.Http;
using System.Threading.Tasks;
using GR.Core.Extensions;

namespace Telemedicine.Extensions
{
    public static class HttpExtensions
    {
        public static async Task<HttpResponseMessage> SendFormUrlEncodedAsync<T>(
            this HttpClient client,
            HttpMethod method,
            string url,
            T value)
        {
            var dict = value.ToDictionary<string>();
            using (var content = new FormUrlEncodedContent(dict))
            {
                var request = new HttpRequestMessage(method, url)
                {
                    Content = content
                };

                return await client.SendAsync(request);
            }
        }
    }
}