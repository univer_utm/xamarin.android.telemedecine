﻿using System;
using System.Drawing;
using System.IO;

namespace Telemedicine.Extensions
{
    public static class BitmapExtensions
    {
        public static Bitmap Base64StringToBitmap(this string
            base64String)
        {
            Bitmap bmpReturn = null;


            var byteBuffer = Convert.FromBase64String(base64String);
            var memoryStream = new MemoryStream(byteBuffer);


            memoryStream.Position = 0;


            bmpReturn = (Bitmap)Bitmap.FromStream(memoryStream);


            memoryStream.Close();
            memoryStream = null;
            byteBuffer = null;


            return bmpReturn;
        }
    }
}