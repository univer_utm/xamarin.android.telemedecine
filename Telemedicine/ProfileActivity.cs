﻿using Android.App;
using Android.OS;
using GR.Core.Helpers;
using Telemedicine.Helpers;

namespace Telemedicine
{
    [Activity(Label = "ProfileActivity")]
    public class ProfileActivity : BaseNavigationActivity
    {
        private readonly TelemedicineHttpClient _client;

        public ProfileActivity()
        {
            _client = IoC.Resolve<TelemedicineHttpClient>();
        }

        protected override int LayoutId => Resource.Layout.ProfileLayout;

        protected override int ActiveMenuId => Resource.Id.navigation_profile;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            var profileRequest = await _client.GetProfileAsync();
        }
    }
}