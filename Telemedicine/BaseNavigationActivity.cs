﻿using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using GR.Core.Helpers;
using System;
using Telemedicine.Helpers;

namespace Telemedicine
{
    public abstract class BaseNavigationActivity : AppCompatActivity, BottomNavigationView.IOnNavigationItemSelectedListener
    {
        private BottomNavigationView bottomNavigationView;
        protected abstract int LayoutId { get; }
        protected abstract int ActiveMenuId { get; }
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            IoC.Resolve<DrawHelper>().SetBarColor(this, Settings.BarColor);
            SetContentView(LayoutId);
            var navigation = FindViewById<BottomNavigationView>(Resource.Id.navigation);
            navigation.SetOnNavigationItemSelectedListener(this);
            bottomNavigationView = FindViewById<BottomNavigationView>(Resource.Id.navigation);

            // bottomNavigationView.SelectedItemId = ActiveMenuId;
            bottomNavigationView.Menu.FindItem(ActiveMenuId).SetChecked(true);
            //GetItem(1).SetChecked(true);
        }

        public bool OnNavigationItemSelected(IMenuItem item)
        {
            bottomNavigationView = FindViewById<BottomNavigationView>(Resource.Id.navigation);
            try
            {
                switch (item.ItemId)
                {
                    case Resource.Id.navigation_home:
                        var homeIntent = new Intent(this, typeof(MainActivity));
                        StartActivity(homeIntent);

                        return true;
                    case Resource.Id.navigation_notifications:
                        var notIntent = new Intent(this, typeof(ApproveNotificationActivity));
                        StartActivity(notIntent);

                        return true;

                    case Resource.Id.navigation_add:
                        var addIntent = new Intent(this, typeof(DoctorListActivity));
                        StartActivity(addIntent);
                        return false;

                    case Resource.Id.navigation_schedule:

                        return true;

                    case Resource.Id.navigation_profile:
                        var intent = new Intent(this, typeof(ProfileActivity));
                        StartActivity(intent);
                        return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return false;
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}