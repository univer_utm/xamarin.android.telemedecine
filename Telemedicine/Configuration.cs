﻿using Castle.MicroKernel.Registration;
using GR.Core.Helpers;
using Telemedicine.Helpers;

namespace Telemedicine
{
    public static class Configuration
    {
        public static void RegisterServices()
        {
            IoC.Container.Register(Component.For<TelemedicineHttpClient>().LifeStyle.Singleton);
            IoC.Container.Register(Component.For<DrawHelper>());
        }
    }
}