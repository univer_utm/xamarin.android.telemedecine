﻿namespace Telemedicine
{
    public static class Settings
    {
        public const string URL = "http://81.180.72.17/";
        public static string Token = "";
        public static string BarColor = "#45e387";

        public static class Routes
        {
            public const string LoginPath = "api/Login/UserAuth";
            public const string RegisterPath = "api/Register/UserReg";
            public const string ProfilePath = "api/Profile/GetProfile";
            public const string DoctorListPath = "api/Doctor/GetDoctorList";
            public const string DoctorByIdPath = "api/Doctor/GetDoctor";
            public const string AddConsultationPath = "api/Doctor/AddConsultation";
        }
    }
}