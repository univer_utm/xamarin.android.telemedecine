﻿using System.Linq;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Widget;
using GR.Core.Helpers;
using Telemedicine.Helpers;

namespace Telemedicine
{
    [Activity(Label = "Doctor List", Theme = "@style/AppTheme", ScreenOrientation = ScreenOrientation.Portrait)]
    public class DoctorListActivity : BaseNavigationActivity
    {
        /// <summary>
        /// Client
        /// </summary>
        private readonly TelemedicineHttpClient _client;

        /// <summary>
        /// List view
        /// </summary>
        private ListView _list;

        public DoctorListActivity()
        {
            _client = IoC.Resolve<TelemedicineHttpClient>();
        }

        /// <summary>
        /// Layout id
        /// </summary>
        protected override int LayoutId => Resource.Layout.DoctorList;

        protected override int ActiveMenuId => Resource.Id.navigation_add;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            var doctorsRequest = await _client.GetDoctorListAsync();
            if (!doctorsRequest.IsSuccess) return;
            _list = FindViewById<ListView>(Resource.Id.list);
            _list.Adapter = new DoctorListAdapter(this, Resource.Layout.DoctorListItem, doctorsRequest.Result.ToList());
        }
    }
}