﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Widget;
using GR.Core.Helpers;
using Telemedicine.Helpers;

namespace Telemedicine
{
    [Activity(Label = "Welcome", ScreenOrientation = ScreenOrientation.Portrait)]
    public class Welcome : Activity
    {

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            IoC.Resolve<DrawHelper>().SetBarColor(this, Settings.BarColor);
            SetContentView(Resource.Layout.Welcome);
            var loginBtn = FindViewById<Button>(Resource.Id.login);
            loginBtn.Click += (sender, args) =>
            {
                var intent = new Intent(this, typeof(Login));
                StartActivity(intent);
            };

            var signupBtn = FindViewById<Button>(Resource.Id.signup);
            signupBtn.Click += (sender, args) =>
            {
                var intent = new Intent(this, typeof(Signup));
                StartActivity(intent);
            };
        }
    }
}